package model

import "time"

type Config struct {
    MqttAddress              string
    MqttClientID             string
    MqttUsername             string
    MqttPassword             string
    BurglarAlarmControlTopic string // jim1/cmd/app/homesec
    BurglarArmDelay          time.Duration
    ApiTopic                 string
    Observables              []Observable
    Sirens                   []Siren
    ObservableDrivers        []ObservableDriver
    SirenDrivers             []SirenDriver
    SensorHeartbeatInterval  int
    MinimumBatteryLevel      int
}

func (conf * Config) GetObservableDriverById(id string) *ObservableDriver {
    for i := range conf.ObservableDrivers {
        if conf.ObservableDrivers[i].Id == id {
            return &conf.ObservableDrivers[i]
        }
    }
    return nil
}

func (conf * Config) GetSirenDriverById(id string) *SirenDriver {
    for i := range conf.SirenDrivers {
        if conf.SirenDrivers[i].Id == id {
            return &conf.SirenDrivers[i]
        }
    }
    return nil
}

type Observable struct {
    Id string
    IsPerimeter bool
    Name string
    ThingAddress string
    ServiceTopic string
    Location string
    Properties map[string]string
    DriverId string
}

type Siren struct {
    Id string
    Topic string
    DriverId string
}

type ObservableDriver struct {
    Id string
    Name string
    Service string
    MsgType string
    ValueType string
    OnValue string
    OffValue string
    SensorType string
    CheckValue bool
}

type SirenDriver struct {
    Id string
    Name string
    Service string
    MsgType string
    ValueType string
    BurglarSituationSound string
    ArmAwaySound string
    ArmStaySound string
    DisarmedSound string
    OffSiren string
}
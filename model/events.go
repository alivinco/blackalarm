package model

const (

)

type Event struct {
	IsPerimeter bool
	Labels []string
	Location string
	SensorType string
	Topic string
	MsgType string
}
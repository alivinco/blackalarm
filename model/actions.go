package model
import (
	msgTrans "github.com/alivinco/iotmsglibgo/transport"
	log "github.com/Sirupsen/logrus"
)
type Actions struct {
	config * Config
	mqttTransport * msgTrans.MqttTransport
}
func (ba *Actions) BeepSiren() {
	log.Info("Beeeeping Siren.")
}

func (ba *Actions) ControlSiren(turnOn bool) {
	if turnOn {
		log.Info("Turning ON siren")
	} else {
		log.Info("Turning OFF siren")
	}

}

func (ba *Actions) SendNotification(message string) {
	log.Infof("Sending notification %s", message)
}

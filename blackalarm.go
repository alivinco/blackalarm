package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/alivinco/blackalarm/brain"

	"github.com/alivinco/blackalarm/eventbus"
	"github.com/alivinco/blackalarm/model"
	"os"
	"os/signal"
	"syscall"
	"io/ioutil"
	"encoding/json"
)

func loadConfigs() (*model.Config , error) {

	fileData , err := ioutil.ReadFile("alarm.json")
	if err != nil {
		log.Error("Can't open configuration file")
		generateConfigTemplate()
		return nil,err;
	}
	conf := model.Config{}
	err = json.Unmarshal(fileData, &conf)
	if err != nil {
		log.Error("Can't decode configuration file . Error :",err)
		return nil,err
	}
	return &conf , nil
}

func generateConfigTemplate() error{
	observ := []model.Observable{{}}
	observDrivers :=  []model.ObservableDriver{{}}
	sirens := []model.Siren{{}}
	sirenDrivers := []model.SirenDriver{{}}

	conf := model.Config{
		ApiTopic:                 "jim1/cmd/app/blackalarm",
		BurglarAlarmControlTopic: "jim1/cmd/app/blackalarm",
		Observables:              observ,
		MqttAddress :"",
		MqttClientID :"",
		MqttUsername :"",
		MqttPassword :"",
		ObservableDrivers:observDrivers,
		Sirens:sirens,
		SirenDrivers:sirenDrivers,

	}
	fileData , err := json.Marshal(conf)
	if err != nil {
		return err
	}
	ioutil.WriteFile("alarm.json",fileData,0777)
	return nil

}

func main() {
	log.SetLevel(log.DebugLevel)
	conf ,_ := loadConfigs()
	burglarAlarm := brain.NewBurglarAlarm()

	router := eventbus.NewMsgRouter(burglarAlarm, conf)
	router.Init()

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		log.Info(sig)
		done <- true
	}()
	log.Info("Awaiting signal")
	<-done
	log.Info("exiting")

}

package brain

import (
	"testing"
	"fmt"
	"time"
	"github.com/alivinco/blackalarm/model"
)

func TestBurglarAlarm_InitFsm(t *testing.T) {
     	ba := NewBurglarAlarm()
	ba.InitFsm()
	err := ba.SetMode("ARMING_AWAY")
	if err != nil {
		fmt.Println(err)
	}
	time.Sleep(time.Second*6)
	//fmt.Println(ba.FSM.Current())
	err = ba.SetMode("DISARMING")
	if err != nil {
		fmt.Println(err)
	}

}

func TestBurglarAlarm_AlarmSituation(t *testing.T) {
	ba := NewBurglarAlarm()
	ba.InitFsm()
	err := ba.SetMode("ARMING_AWAY")
	if err != nil {
		fmt.Println(err)
	}
	time.Sleep(time.Second*6)
	evt := model.Event{IsPerimeter:true,SensorType:"door"}
	ba.OnEvent(&evt)
	//time.Sleep(time.Second)
	if ba.FSM.Current() != "ALARM_SITUATION" {
		t.Error("Wrong state.")
	}

}

func TestBurglarAlarm_CeaseAlarmSituation(t *testing.T) {
	ba := NewBurglarAlarm()
	ba.InitFsm()
	err := ba.SetMode("ARMING_AWAY")
	if err != nil {
		fmt.Println(err)
	}
	time.Sleep(time.Second*6)
	evt := model.Event{IsPerimeter:true,SensorType:"door"}
	ba.OnEvent(&evt)
	ba.SetMode("CEASE_ALARM")
	if ba.FSM.Current() != "ARMED" {
		t.Error("Wrong state.")
	}

}
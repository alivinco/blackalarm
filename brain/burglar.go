package brain

import (
	log "github.com/Sirupsen/logrus"
	"github.com/alivinco/blackalarm/model"
	"github.com/looplab/fsm"
	"time"
)

const (
	ARMED_TYPE_AWAY = 1
	ARMED_TYPE_STAY = 2
)

type BurglarAlarm struct {
	FSM         *fsm.FSM
	armType     string
	alarmEvent  *model.Event
	timer       *time.Timer
	actions	    model.Actions
	configs     *model.Config
}

func NewBurglarAlarm(actions model.Actions , configs * model.Config) *BurglarAlarm {
	ba := BurglarAlarm{actions:actions,configs:configs}
	return &ba

}

func (ba *BurglarAlarm) InitFsm() {
	ba.FSM = fsm.NewFSM(
		"DISARMED",
		fsm.Events{
			{Name: "ARMING_AWAY", Src: []string{"DISARMED"}, Dst: "ENTRY_EXIT_TIME"},
			{Name: "ARMING_STAY", Src: []string{"DISARMED"}, Dst: "ENTRY_EXIT_TIME"},
			{Name: "ARMING_INSTANT", Src: []string{"DISARMED"}, Dst: "ARMED"},
			{Name: "TIMER", Src: []string{"ENTRY_EXIT_TIME"}, Dst: "ARMED"},
			{Name: "TRIGGER_ALARM_SITUATION", Src: []string{"ARMED"}, Dst: "ALARM_SITUATION"},
			{Name: "CEASE_ALARM", Src: []string{"ALARM_SITUATION"}, Dst: "ARMED"},
			{Name: "DISARMING", Src: []string{"ARMED", "ENTRY_EXIT_TIME"}, Dst: "DISARMED"},
		},
		fsm.Callbacks{
			"ENTRY_EXIT_TIME": func(e *fsm.Event) {
				ba.armType = e.Event
				log.Info("Starting ARM TIMER")
				ba.startArmDelayTimer("TIMER")
				ba.actions.BeepSiren()
			},
			"ARMED": func(e *fsm.Event) {
				log.Info("ARMED")
				ba.actions.BeepSiren()
				ba.actions.SendNotification("Home is ARMED")

			},
			"ALARM_SITUATION": func(e *fsm.Event) {

				log.Info("!!!!!ALARM SITUATION!!!!!!")
				ba.actions.ControlSiren(true)
				ba.actions.SendNotification("")

			},
			"before_CEASE_ALARM": func(e *fsm.Event) {

				log.Info("CEASE_ALARM")
				ba.actions.ControlSiren(false)

			},
			"DISARMED": func(e *fsm.Event) {
				log.Info("DISARMED")
				ba.timer.Stop()

			},
		},
	)

}

func (ba *BurglarAlarm) startArmDelayTimer(armType string) {
	ba.timer = time.NewTimer(time.Second * ba.configs.BurglarArmDelay)
	go func() {
		<-ba.timer.C
		err := ba.FSM.Event(armType)
		if err != nil {
			log.Error(err)
		}
	}()
}


func (ba *BurglarAlarm) GetState() string {
	return ba.FSM.Current()
}

func (ba *BurglarAlarm) SetMode(mode string) error {
	return ba.FSM.Event(mode)
}

func (ba *BurglarAlarm) OnEvent(evt *model.Event) {
	log.Infof("Current state :",ba.FSM.Current())
	if ba.FSM.Current() == "ARMED" {
		if ba.armType == "ARMING_AWAY" {
			log.Infof("Alarm situation triggered by sendor at location = %s , topic = %s , msg type = %s", evt.Location, evt.Topic, evt.MsgType)
			ba.alarmEvent = evt
			ba.FSM.Event("TRIGGER_ALARM_SITUATION")

		} else if ba.armType == "ARMING_STAY" {
			if evt.IsPerimeter {
				log.Infof("Alarm situation triggered by sendor at location = %s , topic = %s , msg type = %s", evt.Location, evt.Topic, evt.MsgType)
				ba.alarmEvent = evt
				ba.FSM.Event("TRIGGER_ALARM_SITUATION")

			}

		}
	}
}


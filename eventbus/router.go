package eventbus
import (
	msgTrans "github.com/alivinco/iotmsglibgo/transport"
	iotm "github.com/alivinco/iotmsglibgo"
	log "github.com/Sirupsen/logrus"
	"github.com/alivinco/blackalarm/brain"
	"github.com/alivinco/blackalarm/model"
	"fmt"
	"strings"
)

type MsgRouter struct {
	mqttTransport msgTrans.MqttTransport
	burglarAlrm *brain.BurglarAlarm
	config *model.Config

}

func NewMsgRouter(burglarAlrm *brain.BurglarAlarm,config *model.Config)*MsgRouter {
	router := MsgRouter{burglarAlrm:burglarAlrm,config:config}
	return &router
}

func (md *MsgRouter) Init() {
	mqttTransport := msgTrans.NewMqttTransport("tcp://localhost:1883", "blackalarm", "", "", true, false)
	mqttTransport.SetMessageHandler(md.OnMessage)
	mqttTransport.Start()
	mqttTransport.Subscribe("#", 0)
}

func (md *MsgRouter) OnMessage(topic string, iotMsg *iotm.IotMsg, domain string) {
	log.Infof("New message from topic %s ",iotMsg )
	msgType := fmt.Sprintf("%s.%s",iotMsg.Class,iotMsg.SubClass)
	switch topic {
	case md.config.ApiTopic :
		md.api(topic,msgType,iotMsg)
	case md.config.BurglarAlarmControlTopic :
		md.burglarAlarmControl(topic,msgType ,iotMsg )
	default:
		for i:= range md.config.Observables {
			observableDriver := md.config.GetObservableDriverById(md.config.Observables[i].Id)
			if md.config.Observables[i].ServiceTopic == topic && observableDriver.MsgType == msgType  {
				md.transformEvent(topic ,msgType ,iotMsg , &md.config.Observables[i])
			}
		}
	}
}

func (md *MsgRouter) transformEvent(topic string,msgType string ,iotMsg *iotm.IotMsg , observ *model.Observable  ) {
	observableDriver := md.config.GetObservableDriverById(observ.Id)
	evt := model.Event{IsPerimeter:observ.IsPerimeter,SensorType:observableDriver.SensorType,Location:observ.Location,Topic:topic,MsgType:msgType}
	md.burglarAlrm.OnEvent(&evt)
}

func (md *MsgRouter) burglarAlarmControl(topic string,msgType string,iotMsg *iotm.IotMsg ) {
	mode := strings.ToUpper(iotMsg.GetDefaultStr())
	log.Infof("Setting home to state : %s",mode)
	md.burglarAlrm.SetMode(mode);

}

func (md *MsgRouter) api(topic string ,msgType string ,iotMsg *iotm.IotMsg ) {

	if msgType == "blackalarm.get_burglar_state"{

	}


}